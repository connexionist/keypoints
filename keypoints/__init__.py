import logging.config
import yaml

with open('logging.yml', mode='rt') as f:
    logging_config = yaml.safe_load(f.read())
    logging.config.dictConfig(logging_config)

import keypoints.config
from fastapi import FastAPI, Form, Path, Query, File, UploadFile, Response, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse, PlainTextResponse
from typing import Union, Optional
import os
from datetime import datetime


def create_app() -> FastAPI:
    app = FastAPI(title="keypoints")
    return app
